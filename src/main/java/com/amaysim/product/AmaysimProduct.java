package com.amaysim.product;

public interface AmaysimProduct 
{	
	public void setQuantity(int quantity);
	
	public int  getQuantity();
	
	public String getProductName();
	
	public float getCost();
}
