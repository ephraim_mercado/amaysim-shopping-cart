package com.amaysim.product;

public class UltMedium implements AmaysimProduct
{
	public static final String productCode = "ult_medium";
	public static final String productName = "Unlimited 2GB";
	public static float price = 29.90F;
	public int quantity;
	
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public String getProductName()
	{
		return productName;
	}
	
	public float getCost()
	{
		return price * quantity;
	}
}
