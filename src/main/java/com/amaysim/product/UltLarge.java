package com.amaysim.product;

public class UltLarge implements AmaysimProduct
{
	public static final String productCode = "ult_large";
	public static final String productName = "Unlimited 5GB";
	public static float price = 44.90F;
	public int quantity;
	
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public String getProductName()
	{
		return productName;
	}
	
	public float getCost()
	{
		if (quantity > 3)
		{
			return 39.90F * quantity;
		}
		else
		{
			return price * quantity;
		}
	}
}
