package com.amaysim.product;

public class OneGB implements AmaysimProduct
{
	public static final String productCode = "1gb";
	public static final String productName = "1 GB Data-pack";
	public static float price = 9.90F;
	public int quantity;
	
	public OneGB()
	{
		quantity++;
	}
	
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public String getProductName()
	{
		return productName;
	}
	
	public float getCost()
	{
		return price * quantity;
	}
}
