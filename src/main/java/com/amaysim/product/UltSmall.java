package com.amaysim.product;

public class UltSmall implements AmaysimProduct
{
	public static final String productCode = "ult_small";
	public static final String productName = "Unlimited 1GB";
	public static float price = 24.90F;
	public int quantity;
	
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public String getProductName()
	{
		return productName;
	}
	
	public float getCost()
	{
		if (quantity == 3)
		{
			return price * 2;
		}
		else
		{
			return price * quantity;
		}
	}
}
