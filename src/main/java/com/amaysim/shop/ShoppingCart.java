package com.amaysim.shop;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amaysim.product.AmaysimProduct;
import com.amaysim.product.OneGB;
import com.amaysim.product.ProductType;
import com.amaysim.product.UltLarge;
import com.amaysim.product.UltMedium;
import com.amaysim.product.UltSmall;

public class ShoppingCart
{
	
	private static final String PROMO_CODE_10_OFF = "I<3AMAYSIM"; 
	List<AmaysimProduct> items = new ArrayList<AmaysimProduct>();
	List<AmaysimProduct> totalItems = new ArrayList<AmaysimProduct>();
	private String promoCode;
	
	public ShoppingCart(Map<ProductType, Float> pricingRules)
	{
		UltSmall.price = pricingRules.get(ProductType.ULTSMALL);
		UltMedium.price = pricingRules.get(ProductType.ULTMEDIUM);
		UltLarge.price = pricingRules.get(ProductType.ULTLARGE);
		OneGB.price = pricingRules.get(ProductType.ONEGB);
	}
	
	public float total()
	{
		float total = 0;
		
		for (int i = 0; i < items.size(); i++)
		{
			total += items.get(i).getCost();
		}
		
		if (promoCode != null && promoCode.equalsIgnoreCase(PROMO_CODE_10_OFF))
		{
			float discount = total * 0.10F;
			total -= discount;
		}
		
		return total;
	}
	
	public List<AmaysimProduct> items()
	{
		return items;
	}
	
	public List<AmaysimProduct> totalItems()
	{
		return totalItems;
	}
	
	public void add(AmaysimProduct item) 
	{
		items.add(item);
		totalItems.add(item);
		if (item instanceof UltMedium)
		{
			OneGB oneGb = new OneGB();
			OneGB.price  = 0;
			oneGb.setQuantity(item.getQuantity());
			totalItems.add(oneGb);
		}
	}
	
	public void add(AmaysimProduct item, String promoCode)
	{
		items.add(item);
		totalItems.add(item);
		if (item instanceof UltMedium)
		{
			OneGB oneGb = new OneGB();
			OneGB.price  = 0;
			oneGb.setQuantity(item.getQuantity());
			totalItems.add(oneGb);
		}
		this.promoCode = promoCode;
	}
	
	public String getPromoCode()
	{
		return promoCode;
	}
}	
