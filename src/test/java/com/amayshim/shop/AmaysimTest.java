package com.amayshim.shop;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.amaysim.product.AmaysimProduct;
import com.amaysim.product.OneGB;
import com.amaysim.product.ProductType;
import com.amaysim.product.UltLarge;
import com.amaysim.product.UltMedium;
import com.amaysim.product.UltSmall;
import com.amaysim.shop.ShoppingCart;

public class AmaysimTest 
{

	@Test
	public void testScenario1() 
	{
		Map<ProductType, Float> pricingRules = new HashMap<ProductType, Float>();
		pricingRules.put(ProductType.ULTSMALL, 24.90F);
		pricingRules.put(ProductType.ULTMEDIUM, 29.90F);
		pricingRules.put(ProductType.ULTLARGE, 44.90F);
		pricingRules.put(ProductType.ONEGB, 9.90F);
		ShoppingCart cart = new ShoppingCart(pricingRules);
		UltSmall small = new UltSmall();
		small.setQuantity(3);
		cart.add(small);
		UltLarge large = new UltLarge();
		large.setQuantity(1);
		cart.add(large);
		System.out.println("========================================");
		System.out.println("Scenario 1");
		System.out.println("Items Added");
		for (int i = 0; i < cart.items().size(); i++)
		{
			AmaysimProduct product = cart.items().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Total: " + cart.total());
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Items");
		for (int i = 0; i < cart.items().size(); i++)
		{
			AmaysimProduct product = cart.totalItems().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("========================================");
	}
	
	@Test
	public void testScenario2() 
	{
		Map<ProductType, Float> pricingRules = new HashMap<ProductType, Float>();
		pricingRules.put(ProductType.ULTSMALL, 24.90F);
		pricingRules.put(ProductType.ULTMEDIUM, 29.90F);
		pricingRules.put(ProductType.ULTLARGE, 44.90F);
		pricingRules.put(ProductType.ONEGB, 9.90F);
		ShoppingCart cart = new ShoppingCart(pricingRules);
		UltSmall small = new UltSmall();
		small.setQuantity(2);
		cart.add(small);
		UltLarge large = new UltLarge();
		large.setQuantity(4);
		cart.add(large);
		System.out.println("========================================");
		System.out.println("Scenario 2");
		System.out.println("Items Added");
		for (int i = 0; i < cart.items().size(); i++)
		{
			AmaysimProduct product = cart.items().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Total: " + cart.total());
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Items");
		for (int i = 0; i < cart.items().size(); i++)
		{
			AmaysimProduct product = cart.totalItems().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("========================================");
	}
	
	@Test
	public void testScenario3() 
	{
		Map<ProductType, Float> pricingRules = new HashMap<ProductType, Float>();
		pricingRules.put(ProductType.ULTSMALL, 24.90F);
		pricingRules.put(ProductType.ULTMEDIUM, 29.90F);
		pricingRules.put(ProductType.ULTLARGE, 44.90F);
		pricingRules.put(ProductType.ONEGB, 9.90F);
		ShoppingCart cart = new ShoppingCart(pricingRules);
		UltSmall small = new UltSmall();
		small.setQuantity(1);
		cart.add(small);
		UltMedium medium = new UltMedium();
		medium.setQuantity(2);
		cart.add(medium);
		System.out.println("========================================");
		System.out.println("Scenario 3");
		System.out.println("Items Added");
		for (int i = 0; i < cart.items().size(); i++)
		{
			AmaysimProduct product = cart.items().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Total: " + cart.total());
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Items");
		for (int i = 0; i < cart.totalItems().size(); i++)
		{
			AmaysimProduct product = cart.totalItems().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("========================================");
	}
	
	@Test
	public void testScenario4() 
	{
		Map<ProductType, Float> pricingRules = new HashMap<ProductType, Float>();
		pricingRules.put(ProductType.ULTSMALL, 24.90F);
		pricingRules.put(ProductType.ULTMEDIUM, 29.90F);
		pricingRules.put(ProductType.ULTLARGE, 44.90F);
		pricingRules.put(ProductType.ONEGB, 9.90F);
		ShoppingCart cart = new ShoppingCart(pricingRules);
		UltSmall small = new UltSmall();
		small.setQuantity(1);
		cart.add(small);
		OneGB oneGb = new OneGB();
		oneGb.setQuantity(1);
		cart.add(oneGb, "I<3AMAYSIM");
		System.out.println("========================================");
		System.out.println("Scenario 4");
		System.out.println("Items Added");
		for (int i = 0; i < cart.items().size(); i++)
		{
			AmaysimProduct product = cart.items().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		if (cart.getPromoCode().equalsIgnoreCase("I<3AMAYSIM"))
			System.out.println(cart.getPromoCode() + " Promo Applied");
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Total: " + cart.total());
		System.out.println("----------------------------------------");
		System.out.println("Expected Cart Items");
		for (int i = 0; i < cart.totalItems().size(); i++)
		{
			AmaysimProduct product = cart.totalItems().get(i);
			System.out.println(product.getQuantity() + " x " + product.getProductName());
		}
		System.out.println("========================================");
	}

}
